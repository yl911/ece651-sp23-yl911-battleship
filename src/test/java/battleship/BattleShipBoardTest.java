package battleship;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BattleShipBoardTest {
    @Test
    public void test_width_and_height() {
        Board<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
        assertEquals(10, b1.getWidth());
        Assertions.assertEquals(20, b1.getHeight());
    }
    @Test
    public void test_invalid_dimensions() {
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(10, 0, 'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(0, 20, 'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(10, -5, 'X'));
        assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(-8, 20, 'X'));
    }
    private <T> void checkWhatIsAtBoard(BattleShipBoard<T> b, T[][] expected) {
        for (int i = 0; i < b.getHeight(); i++) {
            for (int j = 0; j < b.getWidth(); j++) {
                // assertEquals(null, );
                assertNull(b.whatIsAtForSelf(new Coordinate(i, j)));
            }
        }
        checkTryAddShip(b, expected);
    }
    private <T> void checkTryAddShip (BattleShipBoard<T> b, T[][] expected) {
        // Ship<T> sampleShip = (Ship<T>) new BasicShip();
        Ship<T> sampleShip = (Ship<T>) new RectangleShip<Character>(new Coordinate("B7"), 's', '*');
        String tryRes = b.tryAddShip(sampleShip);
        assertNull(tryRes);
        assertEquals('s', b.whatIsAtForSelf(new Coordinate("B7")));
    }
    @Test
    public void test_tryAddShip() {
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
        Character[][] expectedRes = new Character[10][20];
        expectedRes[1][7] = 's';
        checkWhatIsAtBoard(b1, expectedRes);
    }
    @Test
    public void test_fireAt() {
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
        V1ShipFactory f = new V1ShipFactory();
        Coordinate c1 = new Coordinate(1, 2);
        Coordinate c2 = new Coordinate(4, 5);
        Placement v1_2 = new Placement(c1, 'V');
        Ship<Character> sh = f.makeBattleship(v1_2);
        b1.tryAddShip(sh);
        assertNull(b1.fireAt(c2));
        assertTrue(b1.enemyMisses.contains(c2));
        assertEquals('X', b1.whatIsAtForEnemy(c2));
        Ship<Character> firedShip = b1.fireAt(c1);
        assertSame (sh, firedShip);
        assertTrue(firedShip.wasHitAt(c1));
    }
    @Test
    public void test_AreShipsAllSunk() {
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
        V1ShipFactory f = new V1ShipFactory();
        Coordinate c1 = new Coordinate(1, 2);
        Coordinate c2 = new Coordinate(4, 5);
        Placement v1_2 = new Placement(c1, 'V');
        Placement v4_5 = new Placement(c2, 'V');
        Ship<Character> sh1 = f.makeSubmarine(v1_2);
        Ship<Character> sh2 = f.makeSubmarine(v4_5);
        b1.tryAddShip(sh1);
        b1.tryAddShip(sh2);
        b1.fireAt(c1);
        b1.fireAt(new Coordinate(2, 2));
        b1.fireAt(c2);
        b1.fireAt(new Coordinate(5, 5));
        assertTrue(sh1.isSunk());
        assertTrue(sh2.isSunk());
        assertTrue(b1.AreShipsAllSunk());
    }
}
