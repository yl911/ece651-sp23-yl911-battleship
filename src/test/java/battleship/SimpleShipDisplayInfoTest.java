package battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SimpleShipDisplayInfoTest {
    @Test
    public void test_getInfo () {
        SimpleShipDisplayInfo<Character> s =
                new SimpleShipDisplayInfo<Character>('s', '*');
        Coordinate where = new Coordinate(1 ,1);
        assertEquals('s', s.getInfo(where, false));
        assertEquals('*', s.getInfo(where, true));
    }
}
