package battleship;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BoardTextViewTest {

    private void emptyBoardHelper(int width, int height, String expectedHeader, String expectedBody){
        Board<Character> b1 = new BattleShipBoard<Character>(width, height, 'X');
        BoardTextView view = new BoardTextView(b1);
        assertEquals(expectedHeader, view.makeHeader());
        assertEquals(expectedBody, view.makeBody((c) -> b1.whatIsAtForSelf(c)));
        String expected = expectedHeader + expectedBody + expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }

    @Test
    public void test_display_empty_2by2() {
        String expectedHeader= "  0|1\n";
        String expectedBody = "A  |  A\n" + "B  |  B\n";
        emptyBoardHelper(2, 2, expectedHeader, expectedBody);
    }
    @Test
    public void test_display_empty_3by2() {
        String expectedHeader= "  0|1|2\n";
        String expectedBody = "A  | |  A\n" + "B  | |  B\n";
        emptyBoardHelper(3, 2, expectedHeader, expectedBody);
    }
    @Test
    public void test_display_empty_3by5() {
        String expectedHeader= "  0|1|2\n";
        String expectedBody = "A  | |  A\n" + "B  | |  B\n"
                + "C  | |  C\n" + "D  | |  D\n" + "E  | |  E\n";
        emptyBoardHelper(3, 5, expectedHeader, expectedBody);
    }
    @Test
    public void test_display_4by3() {
        String expectedHeader= "  0|1|2|3\n";
        String expectedBody = "A  | | |  A\n" + "B  | | |  B\n" + "C  | | |  C\n";
        Board<Character> b1 = new BattleShipBoard<Character>(4, 3, 'X');
        BoardTextView view = new BoardTextView(b1);
        assertEquals(expectedHeader, view.makeHeader());
        assertEquals(expectedBody, view.makeBody((c) -> b1.whatIsAtForSelf(c)));
        String expected = expectedHeader + expectedBody + expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
        b1.tryAddShip(new RectangleShip<Character>(new Coordinate("A2"), 's', '*'));
        expectedBody = "A  | |s|  A\n" + "B  | | |  B\n" + "C  | | |  C\n";
        expected = expectedHeader + expectedBody + expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }
    @Test
    public void test_invalid_board_size() {
        Board<Character> wideBoard = new BattleShipBoard<Character>(11,20, 'X');
        Board<Character> tallBoard = new BattleShipBoard<Character>(10,27, 'X');
        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(wideBoard));
        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(tallBoard));
    }
    @Test
    public void test_displayEnemyBoard() {
        String myView = "  0|1|2|3\n" +
                        "A  | | |d A\n" +
                        "B s|s| |d B\n" +
                        "C  | | |d C\n" +
                        "  0|1|2|3\n";
        String enemyView =  "  0|1|2|3\n" +
                            "A  |X| |  A\n" +
                            "B s| | |d B\n" +
                            "C  | | |  C\n" +
                            "  0|1|2|3\n";
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(4, 3, 'X');
        BoardTextView view = new BoardTextView(b1);

        V1ShipFactory f = new V1ShipFactory();
        Placement h1_2 = new Placement(new Coordinate(1, 0), 'H');
        Ship<Character> sh1 = f.makeSubmarine(h1_2);
        Placement v0_3 = new Placement(new Coordinate(0, 3), 'V');
        Ship<Character> sh2 = f.makeDestroyer(v0_3);
        b1.tryAddShip(sh1);
        b1.tryAddShip(sh2);

        assertEquals(myView, view.displayMyOwnBoard());

        sh1.recordHitAt(new Coordinate(1, 0));
        sh2.recordHitAt(new Coordinate(1, 3));
        b1.enemyMisses.add(new Coordinate(0, 1));

        assertEquals(enemyView, view.displayEnemyBoard());
    }
}
