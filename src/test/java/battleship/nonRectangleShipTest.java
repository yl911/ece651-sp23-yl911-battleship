package battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class nonRectangleShipTest {
    @Test
    public void test_makeCoords() {
        V2ShipFactory f = new V2ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'U');
        Ship<Character> sh = f.makeBattleship(v1_2);
        Coordinate[] expected = new Coordinate[4];
        expected[0] = new Coordinate(1, 3);
        expected[1] = new Coordinate(2, 2);
        expected[2] = new Coordinate(2, 3);
        expected[3] = new Coordinate(2, 4);
        for (Coordinate coordinate : expected) {
            assertTrue(sh.occupiesCoordinates(coordinate));
        }
        assertEquals("Battleship", sh.getName());
    }
}
