package battleship;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import java.io.*;

import static org.junit.jupiter.api.Assertions.*;

public class TextPlayerTest {
    private TextPlayer createTextPlayer(int w, int h, String inputData, ByteArrayOutputStream bytes) {
        BufferedReader input = new BufferedReader(new StringReader(inputData));
        PrintStream output = new PrintStream(bytes, true);
        Board<Character> board = new BattleShipBoard<Character>(w, h, 'X');
        V1ShipFactory shipFactory = new V1ShipFactory();
        return new TextPlayer("A", board, input, output, shipFactory);
    }
    @Test
    public void test_nullInput () {
        Board<Character> board = new BattleShipBoard<Character>(10, 3, 'X');
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream output = new PrintStream(bytes, true);
        V1ShipFactory shipFactory = new V1ShipFactory();
        TextPlayer player1 = new TextPlayer("A", board, new BufferedReader(new StringReader("")), output, shipFactory);
        String prompt = "--------------------------------------------------------------------------------\n" +
                "Player A where do you want to place a Submarine?\n" +
                "--------------------------------------------------------------------------------\n";
        assertThrows(EOFException.class, () -> player1.readPlacement(prompt));
    }
    private void sampleAppHelper (String sampleInput, Placement[] expected) throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player1 = createTextPlayer(10, 20, "B2V\nC8H\na4v\n", bytes);

        String prompt = "--------------------------------------------------------------------------------\n" +
                        "Player A where do you want to place a Destroyer?\n" +
                        "--------------------------------------------------------------------------------\n";
        for (Placement placement : expected) {
            Placement p = player1.readPlacement(prompt);
            assertEquals(p, placement); //did we get the right Placement back
            assertEquals(prompt, bytes.toString()); //should have printed prompt and newline
            bytes.reset(); //clear out bytes for next time around
        }
    }
    @Test
    void test_read_placement() throws IOException {
        Placement[] expected = new Placement[3];
        expected[0] = new Placement(new Coordinate(1, 2), 'V');
        expected[1] = new Placement(new Coordinate(2, 8), 'H');
        expected[2] = new Placement(new Coordinate(0, 4), 'V');
        sampleAppHelper("B2V\nC8H\na4v\n", expected);
    }

    @Test
    void test_doOnePlacement() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player1 = createTextPlayer(10, 3, "BBB\nB2V\nC8H\na4v\n", bytes);
        for (int i = 0; i < 1; i++) {
            // bytes.reset(); //clear out bytes for next time around
            player1.doOnePlacement("Destroyer", (p)->player1.shipFactory.makeDestroyer(p));
        }
        String prompt = "--------------------------------------------------------------------------------\n" +
                        "Player A where do you want to place a Destroyer?\n" +
                        "--------------------------------------------------------------------------------\n";
        String expectedHeader = "  0|1|2|3|4|5|6|7|8|9\n";
        String expectedBody =
                "A  | | | |d| | | | |  A\n" +
                        "B  | | | |d| | | | |  B\n" +
                        "C  | | | |d| | | | |  C\n";
        /* result before bound check
                "A  | | | |d| | | | |  A\n" +
                "B  | |d| |d| | | | |  B\n" +
                "C  | |d| |d| | | |d|d C\n";
         */
        String s = prompt + "That placement is invalid: it does not have the correct format.\n"
                + prompt + "That placement is invalid: the ship goes off the bottom of the board.\n"
                + prompt + "That placement is invalid: the ship goes off the right of the board.\n";
        String res = s + prompt + expectedHeader + expectedBody + expectedHeader;
        assertEquals(res, bytes.toString());
    }
    @Disabled
    @Test
    public void test_doPlacementPhase() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player1 = createTextPlayer(10, 3, "B2V\na9h\na8h\na8h\na8h\na8h\na8h\na8h\na8h\na8h\n", bytes);
//        for (int i = 0; i < 3; i++) {
//            bytes.reset(); //clear out bytes for next time around
//            player1.doPlacementPhase();
//        }
        player1.doPlacementPhase();
        String expectedHeader = "  0|1|2|3|4|5|6|7|8|9\n";
        String expectedBody1 = "A  | | | | | | | | |  A\n" +
                                "B  | | | | | | | | |  B\n" +
                                "C  | | | | | | | | |  C\n";
        String inst = "--------------------------------------------------------------------------------\n" +
                "Player A: you are going to place the following ships (which are all\n" +
                "rectangular). For each ship, type the coordinate of the upper left\n" +
                "side of the ship, followed by either H (for horizontal) or V (for\n" +
                "vertical).  For example M4H would place a ship horizontally starting\n" +
                "at M4 and going to the right.  You have\n" +
                "\n" +
                "2 \"Submarines\" ships that are 1x2\n" +
                "3 \"Destroyers\" that are 1x3\n" +
                "3 \"Battleships\" that are 1x4\n" +
                "2 \"Carriers\" that are 1x6\n" +
                "--------------------------------------------------------------------------------\n";
        String prompt;
        String expectedBody2 = "A  | | | | | | | | |  A\n" +
                                "B  | |s| | | | | | |  B\n" +
                                "C  | |s| | | | | | |  C\n";
        String res =  expectedHeader + expectedBody1 + expectedHeader + inst;
        for (String s : player1.shipsToPlace) {
            prompt = "--------------------------------------------------------------------------------\n" +
                    "Player A where do you want to place a " + s + "?\n" +
                    "--------------------------------------------------------------------------------\n";
            res += prompt + expectedHeader + expectedBody2 + expectedHeader;
        }
        assertEquals(res, bytes.toString());
    }
    @Test
    public void test_displayMyBoardWithEnemyNextToIt() throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player1 = createTextPlayer(10, 3, "B2v\n", bytes);
        TextPlayer player2 = createTextPlayer(10, 3, "a7h\n", bytes);
        for (int i = 0; i < 1; i++) {
            player1.doOnePlacement("Submarine", (p)->player1.shipFactory.makeSubmarine(p));
        }
        for (int i = 0; i < 1; i++) {
            player2.doOnePlacement("Submarine", (p)->player2.shipFactory.makeSubmarine(p));
        }
        String header1 = "Your ocean";
        String header2 = "B";
        String expected = "     Your ocean                           Player B's ocean\n" +
                          "  0|1|2|3|4|5|6|7|8|9                    0|1|2|3|4|5|6|7|8|9\n" +
                            "A  | | | | | | | | |  A                A  | | | | | | | | |  A\n" +
                            "B  | |s| | | | | | |  B                B  | | | | | | | | |  B\n" +
                            "C  | |s| | | | | | |  C                C  | | | | | | | | |  C\n" +
                            "  0|1|2|3|4|5|6|7|8|9                    0|1|2|3|4|5|6|7|8|9\n";
        assertEquals("\n" + expected, "\n" + player1.displayMyBoardWithEnemyNextToIt(player2.getView(), header1, header2));
    }
}
