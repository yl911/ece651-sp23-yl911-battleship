package battleship;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;

public class RectangleShipTest {
    @Test
    public void test_makeCoords () {
        Coordinate[] expected = new Coordinate[4];
        expected[0] = new Coordinate(1, 2);
        expected[1] = new Coordinate(2, 2);
        expected[2] = new Coordinate(3, 2);
        expected[3] = new Coordinate(4, 2);
        HashSet<Coordinate> ans =  RectangleShip.makeCoords(expected[0], 1, 4);
        for (int i = 0; i < ans.size(); i++) {
            assertTrue(ans.contains(expected[i]));
        }

    }
    @Test
    public void test_makeRectangleShip () {
        Coordinate[] expected = new Coordinate[4];
        expected[0] = new Coordinate(1, 2);
        expected[1] = new Coordinate(2, 2);
        expected[2] = new Coordinate(3, 2);
        expected[3] = new Coordinate(4, 2);
        RectangleShip<Character> sh = new RectangleShip<Character>("submarine", expected[0], 1 , 4, 's', '*');
        for (Coordinate coordinate : expected) {
            assertTrue(sh.occupiesCoordinates(coordinate));
        }
        assertEquals("submarine", sh.getName());
    }
    @Test
    public void test_HitAt() {
        Coordinate c1 = new Coordinate(1, 2);
        Coordinate c2 = new Coordinate(3, 2);
        Coordinate c3 = new Coordinate(1, 3);
        RectangleShip<Character> rs = new RectangleShip<Character>("submarine", c1, 1, 4, 's', '*');
        rs.recordHitAt(c2);
        assertTrue(rs.wasHitAt(c2));
        assertThrows(IllegalArgumentException.class, () -> rs.wasHitAt(c3));
    }
    @Test
    public void test_issunk () {
        Coordinate[] expected = new Coordinate[4];
        expected[0] = new Coordinate(1, 2);
        expected[1] = new Coordinate(2, 2);
        expected[2] = new Coordinate(3, 2);
        expected[3] = new Coordinate(4, 2);
        RectangleShip<Character> sh = new RectangleShip<Character>("submarine", expected[0], 1 , 4, 's', '*');
        for (Coordinate c: expected) {
            assertFalse(sh.isSunk());
            sh.recordHitAt(c);
        }
        assertTrue(sh.isSunk());
    }
    @Test
    public void test_getDisplayInfoAt () {
        Coordinate[] expected = new Coordinate[4];
        expected[0] = new Coordinate(1, 2);
        expected[1] = new Coordinate(2, 2);
        expected[2] = new Coordinate(3, 2);
        expected[3] = new Coordinate(4, 2);
        RectangleShip<Character> sh = new RectangleShip<Character>("submarine", expected[0], 1 , 4, 's', '*');

        sh.recordHitAt(expected[2]);
        assertTrue(sh.wasHitAt(expected[2]));
        assertFalse(sh.wasHitAt(expected[3]));
        assertEquals('*', sh.getDisplayInfoAt(expected[2],true));
        assertEquals('s', sh.getDisplayInfoAt(expected[2],false));
        assertEquals('s', sh.getDisplayInfoAt(expected[3],true));
        assertNull(sh.getDisplayInfoAt(expected[3], false));
    }

    @Test
    public void test_getCoordinates() {
        Coordinate[] expected = new Coordinate[4];
        expected[0] = new Coordinate(1, 2);
        expected[1] = new Coordinate(2, 2);
        expected[2] = new Coordinate(3, 2);
        expected[3] = new Coordinate(4, 2);
        RectangleShip<Character> sh = new RectangleShip<Character>("submarine", expected[0], 1 , 4, 's', '*');
        // the order is not the same as in expected, with expected[0] at the tailer
        Iterator<Coordinate> it = sh.getCoordinates().iterator();
        HashSet<Coordinate> ex = new HashSet<Coordinate>();
        ex.add(expected[0]);
        ex.add(expected[1]);
        ex.add(expected[2]);
        ex.add(expected[3]);
        while (it.hasNext()) {
            Coordinate ans = it.next();
            assertTrue(ex.contains(ans));
        }
    }
}
