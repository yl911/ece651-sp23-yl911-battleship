package battleship;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PlacementTest {
    @Test
    public void test_equals() {
        Coordinate c1 = new Coordinate(1, 2);
        Coordinate c2 = new Coordinate(1, 2);
        Coordinate c3 = new Coordinate(1, 3);
        Coordinate c4 = new Coordinate(3, 2);
        Placement p1 = new Placement(c1, 'v');
        Placement p2 = new Placement(c2, 'V');
        Placement p3 = new Placement(c3, 'H');
        Placement p4 = new Placement(c4, 'h');
        assertEquals(p1, p1);   //equals should be reflexsive
        assertEquals(p1.getCoordinate(), p2.getCoordinate());
        assertEquals(p1.getOrientation(),p2.getOrientation());
        assertEquals(p1, p2);   //different objects bu same contents
        assertNotEquals(p1, p3);  //different contents
        assertNotEquals(p1, p4);
        assertNotEquals(p3, p4);
        assertNotEquals(p1, "(1, 2) V"); //different types
    }
    @Test
    public void test_hashCode() {
        Coordinate c1 = new Coordinate(1, 2);
        Coordinate c2 = new Coordinate(1, 2);
        Coordinate c3 = new Coordinate(0, 3);
        Coordinate c4 = new Coordinate(2, 1);
        Placement p1 = new Placement(c1, 'v');
        Placement p2 = new Placement(c2, 'V');
        Placement p3 = new Placement(c3, 'H');
        Placement p4 = new Placement(c4, 'h');
        assertEquals(p1.hashCode(), p2.hashCode());
        assertNotEquals(p1.hashCode(), p3.hashCode());
        assertNotEquals(p1.hashCode(), p4.hashCode());
    }

    @Test
    public void test_string_constructor_error_cases() {
        assertThrows(IllegalArgumentException.class, () -> new Placement("000"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("AAA"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("@0D"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("[0S"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A/C"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A:C"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A12"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A1C"));
    }

    @Test
    void test_string_constructor_valid_cases() {
        Coordinate c1 = new Coordinate("B3");
        Coordinate c2 = new Coordinate("D5");
        Coordinate c3 = new Coordinate("A9");
        Coordinate c4 = new Coordinate("Z0");

        Placement p1 = new Placement(c1, 'v');
        Placement p2 = new Placement(c2, 'V');
        Placement p3 = new Placement(c3, 'H');
        Placement p4 = new Placement(c4, 'h');
        Placement p5 = new Placement("B3V");
        assertEquals(c1, p1.getCoordinate());
        assertEquals(c2, p2.getCoordinate());
        assertEquals(c3, p3.getCoordinate());
        assertEquals(c4, p4.getCoordinate());
        assertEquals(c1, p5.getCoordinate());
        assertEquals('V', p1.getOrientation());
        assertEquals('V', p2.getOrientation());
        assertEquals('H', p3.getOrientation());
        assertEquals('H', p4.getOrientation());
        assertEquals('V', p5.getOrientation());
    }
}
