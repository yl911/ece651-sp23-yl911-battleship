package battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class NoCollisionRuleCheckerTest {
    @Test
    public void test_NoCollisionRuleChecker() {
        NoCollisionRuleChecker<Character> rc = new NoCollisionRuleChecker<Character>(null);
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, rc, 'X');
        V1ShipFactory f = new V1ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> sh = f.makeBattleship(v1_2);
        b1.tryAddShip(sh);
        Placement h1_1 = new Placement(new Coordinate(1, 1), 'H');
        Ship<Character> sh1 = f.makeBattleship(h1_1);

        Placement v6_2 = new Placement(new Coordinate(6, 2), 'V');
        Ship<Character> sh2 = f.makeBattleship(v6_2);

        assertNotNull(rc.checkPlacement(sh1, b1));
        assertNull   (rc.checkPlacement(sh2, b1));
    }
    @Test
    public void test_combinedTest() {
        NoCollisionRuleChecker<Character> rc =
                new NoCollisionRuleChecker<Character>(new InBoundsRuleChecker<Character>(null));
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, rc, 'X');
        V1ShipFactory f = new V1ShipFactory();

        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> sh = f.makeBattleship(v1_2);
        b1.tryAddShip(sh);
        Placement h1_1 = new Placement(new Coordinate(1, 1), 'H');
        Ship<Character> sh1 = f.makeBattleship(h1_1);
        Placement v6_2 = new Placement(new Coordinate(6, 2), 'V');
        Ship<Character> sh2 = f.makeBattleship(v6_2);

        assertNotNull(rc.checkPlacement(sh1, b1));
        assertNull   (rc.checkPlacement(sh2, b1));

        Placement v19_2 = new Placement(new Coordinate(19, 2), 'V');
        Ship<Character> sh3 = f.makeBattleship(v19_2);
        Placement h1_8 = new Placement(new Coordinate(1, 8), 'H');
        Ship<Character> sh4 = f.makeBattleship(h1_8);

        assertNotNull(b1.getPlacementChecker().checkPlacement(sh3, b1));
        assertNotNull(b1.getPlacementChecker().checkPlacement(sh4, b1));
    }
}
