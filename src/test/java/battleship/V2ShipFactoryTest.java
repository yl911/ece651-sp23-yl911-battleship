package battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class V2ShipFactoryTest {
    private void checkShip(Ship<Character> testShip, String expectedName,
                           char expectedLetter, Coordinate... expectedLocs) {
        assertEquals(expectedName, testShip.getName());
        for (Coordinate c : expectedLocs) {
            assertTrue(testShip.occupiesCoordinates(c));
            assertFalse(testShip.wasHitAt(c));
            assertEquals(expectedLetter, testShip.getDisplayInfoAt(c, true));
            assertNull(testShip.getDisplayInfoAt(c, false));
        }
        assertFalse(testShip.isSunk());
        for (Coordinate c : expectedLocs) {
            testShip.recordHitAt(c);
            assertTrue(testShip.wasHitAt(c));
            assertEquals('*', testShip.getDisplayInfoAt(c, true));
            assertEquals(expectedLetter, testShip.getDisplayInfoAt(c, false));
        }
        assertTrue(testShip.isSunk());
    }
    @Test
    public void test_makeSubmarine () {
        V2ShipFactory f = new V2ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst = f.makeSubmarine(v1_2);
        checkShip(dst, "Submarine", 's',
                new Coordinate(1, 2),
                new Coordinate(2, 2));
        Placement h1_2 = new Placement(new Coordinate(1, 2), 'H');
        dst = f.makeSubmarine(h1_2);
        checkShip(dst, "Submarine", 's',
                new Coordinate(1, 2),
                new Coordinate(1, 3));
    }
    @Test
    public void test_makeDestroyer () {
        V2ShipFactory f = new V2ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst = f.makeDestroyer(v1_2);
        checkShip(dst, "Destroyer", 'd',
                new Coordinate(1, 2),
                new Coordinate(2, 2),
                new Coordinate(3, 2));
        Placement h1_2 = new Placement(new Coordinate(1, 2), 'H');
        dst = f.makeDestroyer(h1_2);
        checkShip(dst, "Destroyer", 'd',
                new Coordinate(1, 2),
                new Coordinate(1, 3),
                new Coordinate(1, 4));
    }
    @Test
    public void test_makeBattleship () {
        V2ShipFactory f = new V2ShipFactory();
        Placement u1_2 = new Placement(new Coordinate(1, 2), 'U');
        Ship<Character> dst = f.makeBattleship(u1_2);
        checkShip(dst, "Battleship", 'b',
                 new Coordinate(1, 3),
                 new Coordinate(2, 2),
                 new Coordinate(2, 3),
                 new Coordinate(2, 4));
        Placement r1_2 = new Placement(new Coordinate(1, 2), 'R');
        dst = f.makeBattleship(r1_2);
        checkShip(dst, "Battleship", 'b',
                new Coordinate(1, 2),
                new Coordinate(2, 2),
                new Coordinate(3, 2),
                new Coordinate(2, 3));
        Placement d1_2 = new Placement(new Coordinate(1, 2), 'D');
        dst = f.makeBattleship(d1_2);
        checkShip(dst, "Battleship", 'b',
                new Coordinate(1, 2),
                new Coordinate(1, 3),
                new Coordinate(1, 4),
                new Coordinate(2, 3));
        Placement l1_2 = new Placement(new Coordinate(1, 2), 'L');
        dst = f.makeBattleship(l1_2);
        checkShip(dst, "Battleship", 'b',
                new Coordinate(1, 3),
                new Coordinate(2, 2),
                new Coordinate(2, 3),
                new Coordinate(3, 3));
    }
    @Test
    public void test_makeCarrier () {
        V2ShipFactory f = new V2ShipFactory();
        Placement u1_2 = new Placement(new Coordinate(1, 2), 'U');
        Ship<Character> dst = f.makeCarrier(u1_2);
        checkShip(dst, "Carrier", 'c',
                new Coordinate(1, 2),
                new Coordinate(2, 2),
                new Coordinate(3, 2),
                new Coordinate(4, 2),
                new Coordinate(3, 3),
                new Coordinate(4, 3),
                new Coordinate(5, 3));
        Placement r1_2 = new Placement(new Coordinate(1, 2), 'R');
        dst = f.makeCarrier(r1_2);
        checkShip(dst, "Carrier", 'c',
                new Coordinate(1, 3),
                new Coordinate(1, 4),
                new Coordinate(1, 5),
                new Coordinate(1, 6),
                new Coordinate(2, 2),
                new Coordinate(2, 3),
                new Coordinate(2, 4));
        Placement d1_2 = new Placement(new Coordinate(1, 2), 'D');
        dst = f.makeCarrier(d1_2);
        checkShip(dst, "Carrier", 'c',
                new Coordinate(1, 2),
                new Coordinate(2, 2),
                new Coordinate(3, 2),
                new Coordinate(2, 3),
                new Coordinate(3, 3),
                new Coordinate(4, 3),
                new Coordinate(5, 3));
        Placement l1_2 = new Placement(new Coordinate(1, 2), 'L');
        dst = f.makeCarrier(l1_2);
        checkShip(dst, "Carrier", 'c',
                new Coordinate(1, 4),
                new Coordinate(1, 5),
                new Coordinate(1, 6),
                new Coordinate(2, 2),
                new Coordinate(2, 3),
                new Coordinate(2, 4),
                new Coordinate(2, 5));
    }
}
