package battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class V1ShipFactoryTest {
    private void checkShip(Ship<Character> testShip, String expectedName,
                           char expectedLetter, Coordinate... expectedLocs) {
        assertEquals(expectedName, testShip.getName());
        for (Coordinate c : expectedLocs) {
            assertTrue(testShip.occupiesCoordinates(c));
            assertFalse(testShip.wasHitAt(c));
            assertEquals(expectedLetter, testShip.getDisplayInfoAt(c, true));
            assertNull(testShip.getDisplayInfoAt(c, false));
        }
        assertFalse(testShip.isSunk());
    }
    @Test
    public void test_makeSubmarine () {
        V1ShipFactory f = new V1ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst = f.makeSubmarine(v1_2);
        checkShip(dst, "Submarine", 's',
                new Coordinate(1, 2),
                new Coordinate(2, 2));
        Placement h1_2 = new Placement(new Coordinate(1, 2), 'H');
        dst = f.makeSubmarine(h1_2);
        checkShip(dst, "Submarine", 's',
                new Coordinate(1, 2),
                new Coordinate(1, 3));
    }
    @Test
    public void test_makeDestroyer () {
        V1ShipFactory f = new V1ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst = f.makeDestroyer(v1_2);
        checkShip(dst, "Destroyer", 'd',
                new Coordinate(1, 2),
                new Coordinate(2, 2),
                new Coordinate(3, 2));
        Placement h1_2 = new Placement(new Coordinate(1, 2), 'H');
        dst = f.makeDestroyer(h1_2);
        checkShip(dst, "Destroyer", 'd',
                new Coordinate(1, 2),
                new Coordinate(1, 3),
                new Coordinate(1, 4));
    }
    @Test
    public void test_makeBattleship () {
        V1ShipFactory f = new V1ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst = f.makeBattleship(v1_2);
        checkShip(dst, "Battleship", 'b',
                new Coordinate(1, 2),
                new Coordinate(2, 2),
                new Coordinate(3, 2),
                new Coordinate(4, 2));
        Placement h1_2 = new Placement(new Coordinate(1, 2), 'H');
        dst = f.makeBattleship(h1_2);
        checkShip(dst, "Battleship", 'b',
                new Coordinate(1, 2),
                new Coordinate(1, 3),
                new Coordinate(1, 4),
                new Coordinate(1, 5));
    }
    @Test
    public void test_makeCarrier () {
        V1ShipFactory f = new V1ShipFactory();
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst = f.makeCarrier(v1_2);
        checkShip(dst, "Carrier", 'c',
                new Coordinate(1, 2),
                new Coordinate(2, 2),
                new Coordinate(3, 2),
                new Coordinate(4, 2),
                new Coordinate(5, 2),
                new Coordinate(6, 2));
        Placement h1_2 = new Placement(new Coordinate(1, 2), 'H');
        dst = f.makeCarrier(h1_2);
        checkShip(dst, "Carrier", 'c',
                new Coordinate(1, 2),
                new Coordinate(1, 3),
                new Coordinate(1, 4),
                new Coordinate(1, 5),
                new Coordinate(1, 6),
                new Coordinate(1, 7));
    }
}
