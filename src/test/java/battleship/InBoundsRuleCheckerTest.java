package battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class InBoundsRuleCheckerTest {
    @Test
    public void test_checkMyRule() {
        V1ShipFactory f = new V1ShipFactory();
        Placement v19_2 = new Placement(new Coordinate(19, 2), 'V');
        Ship<Character> sh = f.makeBattleship(v19_2);
        InBoundsRuleChecker<Character> rc = new InBoundsRuleChecker<Character>(null);
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, rc, 'X');
        assertNotNull(b1.getPlacementChecker().checkPlacement(sh, b1));

        Placement v1_8 = new Placement(new Coordinate(1, 8), 'H');
        Ship<Character> sh1 = f.makeBattleship(v1_8);
        assertNotNull(b1.getPlacementChecker().checkPlacement(sh1, b1));

        Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> sh2 = f.makeBattleship(v1_2);
        assertNull(b1.getPlacementChecker().checkPlacement(sh2, b1));
    }
}
