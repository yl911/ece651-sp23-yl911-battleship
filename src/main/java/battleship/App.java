package battleship;

import java.io.*;

public class App {
    final TextPlayer player1;
    final TextPlayer player2;

    public App (TextPlayer player1, TextPlayer player2) {
        this.player1 = player1;
        this.player2 = player2;
    }
    public void doPlacementPhase() throws IOException {
        player1.doPlacementPhase();
        player2.doPlacementPhase();
    }
    public void doAttackingPhase() throws IOException {
        while (true) {
            player1.playOneTurn(player2.getTheBoard(), player2.getView(), player2.name);
            if (player2.checkWinOrLose(player1.name)) {
                break;
            }
            player2.playOneTurn(player1.getTheBoard(), player1.getView(), player1.name);
            if (player1.checkWinOrLose(player2.name)) {
                break;
            }
        }
    }
    public static void main(String[] args) throws IOException {
//        Board<Character> b = new BattleShipBoard<Character>(10, 20);
//        App app = new App(b, new InputStreamReader(System.in), System.out);
//        app.doOnePlacement();

        Board<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
        Board<Character> b2 = new BattleShipBoard<Character>(10, 20, 'X');
        V1ShipFactory factory1 = new V1ShipFactory();
        V2ShipFactory factory2 = new V2ShipFactory();
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader input1 = new BufferedReader(new FileReader("src/main/resources/input2.txt"));
        PrintStream output = System.out;
        FileOutputStream file = new FileOutputStream("src/main/resources/output2.txt");
        PrintStream output1 = new PrintStream(file, true);
        TextPlayer p1 = new TextPlayer("A", b1, input1, output1, factory2); // System.out
        TextPlayer p2 = new TextPlayer("B", b2, input1, output1, factory2);
        App app = new App(p1, p2);

        app.doPlacementPhase();
        // app.doAttackingPhase();
    }
}
