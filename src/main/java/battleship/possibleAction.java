package battleship;

public class possibleAction {
    protected int numMove;
    protected int numSonarScan;
    protected String typeSigns;
    public possibleAction () {
        this.numMove = 3;
        this.numSonarScan = 3;
        this.typeSigns = "FMS";
    }
    public boolean moveRemain () {
        return numMove > 0;
    }
    public boolean scanRemain () {
        return numSonarScan > 0;
    }
    public String prompt (String playerName) {
        String dashLine = "--------------------------------------------------------------------------------\n";
        String prompt =  "Possible actions for Player " + playerName + ":\n\n" +
                "F Fire at a square\n" +
                "M Move a ship to another square (" + numMove + " remaining)\n" +
                "S Sonar scan (" + numSonarScan + " remaining)\n\n" +
                "Player " + playerName + ", what would you like to do?\n";
        return dashLine + prompt + dashLine;
    }
    public void move () {

    }
    public void sonarScan () {

    }
}
