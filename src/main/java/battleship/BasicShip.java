package battleship;

import java.util.HashMap;
import java.util.HashSet;

public abstract class BasicShip<T> implements Ship<T> {
    // private final Coordinate myLocation;
    HashMap<Coordinate, Boolean> myPieces;
    protected ShipDisplayInfo<T> myDisplayInfo;
    protected ShipDisplayInfo<T> enemyDisplayInfo;

    public BasicShip(Iterable<Coordinate> where, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo) {
        this.myDisplayInfo = myDisplayInfo;
        this.enemyDisplayInfo = enemyDisplayInfo;
        this.myPieces = new HashMap<Coordinate, Boolean>();
        for (Coordinate c: where) {
            myPieces.put(c,false);
        }
    }
    public boolean occupiesCoordinates(Coordinate where) {
        return myPieces.containsKey(where);
    }
    protected void checkCoordinateInThisShip(Coordinate c) throws IllegalArgumentException {
        if (!myPieces.containsKey(c)) {
            throw new IllegalArgumentException("Coordinate " + c + "is not in this ship.");
        }
    }
    public boolean isSunk() {
        for (Boolean wasHitAt: myPieces.values()) {
            if (!wasHitAt) {
                return false;
            }
        }
        return true;
    }
    public void recordHitAt(Coordinate where) throws IllegalArgumentException{
        checkCoordinateInThisShip(where);
        this.myPieces.put(where, true);
    }
    public boolean wasHitAt(Coordinate where) throws IllegalArgumentException{
        checkCoordinateInThisShip(where);
        return this.myPieces.get(where);
    }
    @Override
    public T getDisplayInfoAt(Coordinate where, boolean myShip) throws IllegalArgumentException{
        checkCoordinateInThisShip(where);
        if (myShip) {
            return myDisplayInfo.getInfo(where, wasHitAt(where));
        } else {
            return enemyDisplayInfo.getInfo(where, wasHitAt(where));
        }

    }
    @Override
    public Iterable<Coordinate> getCoordinates() {
        return myPieces.keySet();
    }

}
