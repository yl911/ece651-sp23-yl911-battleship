package battleship;

public class NoCollisionRuleChecker<T> extends PlacementRuleChecker<T> {
    /**
     * theShip does not collide with anything else
     * on theBoard (that all the squares it needs are empty)
     */
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        for (Coordinate ans : theShip.getCoordinates()) {
            if (theBoard.whatIsAtForSelf(ans) != null) {
                return "That placement is invalid: the ship overlaps another ship.\n"; // the body of another ship
            }
        }
        return null;
    }

    public NoCollisionRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }
}
