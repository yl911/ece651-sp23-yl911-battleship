package battleship;

public class Coordinate {
    private final int column; // the index of columns
    private final int row; // the index of rows
    public int getRow() {
        return row;
    }
    public int getColumn() {
        return column;
    }
    public Coordinate (int row, int column) {
        this.row = row;
        this.column = column;
    }
    public Coordinate(String descr) {
        if (descr.length() != 2) {
            throw new IllegalArgumentException("Coordinate must be two characters but is " + descr.length() + " bytes long");
        }
        String descrIgnoredCases = descr.toUpperCase();
        int row    = descrIgnoredCases.charAt(0) - 'A';
        int column = descrIgnoredCases.charAt(1) - '0';
        if (column < 0 || column > 9) {
            throw new IllegalArgumentException("Coordinate's column must be positive and less than 9 but is " + column);
        }
        if (row < 0 || row > 25) {
            throw new IllegalArgumentException("Coordinate's row must be positive and less than 26 but is " + row);
        }
        this.row = row;
        this.column = column;
    }

    @Override
    public boolean equals(Object o) {
        if (o.getClass().equals(this.getClass())) {
            Coordinate c = (Coordinate) o;
            return this.row == c.getRow() && this.column == c.getColumn();
        }
        return false;
    }
    @Override
    public String toString() {
        return "("+row+", " + column+")";
    }
    @Override
    public int hashCode() {
        return toString().hashCode();
    }
}
