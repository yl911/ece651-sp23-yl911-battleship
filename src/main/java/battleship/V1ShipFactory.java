package battleship;

/**
 * For V1, all of these do basically the same thing, but have different values they pass
 * to the constructor for RectangleShip.
 */
public class V1ShipFactory implements AbstractShipFactory<Character>{

    protected Ship<Character> createShip(Placement where, int w, int h, char letter, String name) {

        if (where.getOrientation() == 'V') {
            return new RectangleShip<Character>
                    (name, where.getCoordinate(), w, h, letter, '*');
        } else {// if (where.getOrientation() == 'H')
            return new RectangleShip<Character>
                    (name, where.getCoordinate(), h, w, letter, '*');
        }
    }

    @Override
    public Ship<Character> makeSubmarine(Placement where) {
        return createShip(where, 1, 2, 's', "Submarine");
    }

    @Override
    public Ship<Character> makeDestroyer (Placement where) {
        return createShip(where, 1, 3, 'd', "Destroyer");
    }

    @Override
    public Ship<Character> makeBattleship(Placement where) {
        return createShip(where, 1, 4, 'b', "Battleship");
    }

    @Override
    public Ship<Character> makeCarrier (Placement where) {
        return createShip(where, 1, 6, 'c', "Carrier");
    }
}
