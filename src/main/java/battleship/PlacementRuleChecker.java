package battleship;

public abstract class PlacementRuleChecker<T> {
    private final PlacementRuleChecker<T> next;
    //more stuff
    public PlacementRuleChecker(PlacementRuleChecker<T> next) {
        this.next = next;
    }

    /**
     * Subclasses will override this method to specify how they check their own rule.
     */
    protected abstract String checkMyRule(Ship<T> theShip, Board<T> theBoard);

    /**
     * They don't need to consider any other rule, nor the chaining logic.  Instead,
     * our public method handles chaining rules together
     */
    public String checkPlacement (Ship<T> theShip, Board<T> theBoard) {
        //if we fail our own rule: stop the placement is not legal
        String s = checkMyRule(theShip, theBoard);
        if (s != null) {
            return s;
        }
        //otherwise, ask the rest of the chain.
        if (next != null) {
            s = next.checkPlacement(theShip, theBoard);
            return s;
        }
        //if there are no more rules, then the placement is legal
        return null;
    }
}

