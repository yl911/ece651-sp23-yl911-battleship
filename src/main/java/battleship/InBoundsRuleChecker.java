package battleship;

import java.util.Iterator;

public class InBoundsRuleChecker<T> extends PlacementRuleChecker<T> {
    /**
     * iterate over all the coordinates in theShip and check that they are
     * in bounds on theBoard (i.e. 0 <= row < height and 0<= column < width).
     */
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        for (Coordinate ans : theShip.getCoordinates()) {
            if (0 > ans.getRow()) {
                return "That placement is invalid: the ship goes off the top of the board.\n";
            } else if (ans.getRow() >= theBoard.getHeight()) {
                return "That placement is invalid: the ship goes off the bottom of the board.\n";
            } else if (0 > ans.getColumn()) {
                return "That placement is invalid: the ship goes off the left of the board.\n";
            } else if (ans.getColumn() >= theBoard.getWidth()) {
                return "That placement is invalid: the ship goes off the right of the board.\n";
            }
        }
        return null;
    }

    public InBoundsRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }
}
