package battleship;

import java.util.function.Function;

/**
 * This class handles textual display of
 * a Board (i.e., converting it to a string to show
 * to the user).
 * It supports two ways to display the Board:
 * one for the player's own board, and one for the
 * enemy's board.
 */
public class BoardTextView {
    /**
     * The Board to display
     */
    private final Board<Character> toDisplay;
    /**
     * Constructs a BoardView, given the board it will display.
     * @param toDisplay is the Board to display
     */
    public BoardTextView(Board<Character> toDisplay) {
        this.toDisplay = toDisplay;
        if (toDisplay.getWidth() > 10 || toDisplay.getHeight() > 26) {
            throw new IllegalArgumentException(
                    "Board must be no larger than 10x26, but is " + toDisplay.getWidth() + "x" + toDisplay.getHeight());
        }
    }
    protected String displayAnyBoard(Function<Coordinate, Character> getSquareFn) {
        String header = this.makeHeader();
        String body   = this.makeBody  (getSquareFn);
        return header + body + header;
    }
    public String displayEnemyBoard() {
        return displayAnyBoard((c)->toDisplay.whatIsAtForEnemy(c));
    }
    /**
     * Displays an empty board, with header + body + header
     * @return is a String of the board to display
     */
    public String displayMyOwnBoard() {
        return displayAnyBoard((c)->toDisplay.whatIsAtForSelf(c));
    }
    /**
     * This makes the body lines, e.g. A|  |A\n
     * @return the String that is the body/empty for the given board
     */
    String makeBody(Function<Coordinate, Character> getSquareFn) {
        StringBuilder ans = new StringBuilder(); // README shows two spaces at
        for (int row = 0; row < toDisplay.getHeight(); row++) {
            StringBuilder bodyLine = new StringBuilder((char)(row + 65) + " ");
            String sep = ""; //start with nothing to separate, then switch to | to separate
            Character content;
            for (int column = 0; column < toDisplay.getWidth(); column++) {
                bodyLine.append(sep);
                content = getSquareFn.apply(new Coordinate(row, column));
                if (content == null) {
                    bodyLine.append(" ");
                } else {
                    bodyLine.append(content);
                }

                sep = "|";
            }
            bodyLine.append(" " + (char)(row + 65));
            bodyLine.append("\n");
            ans.append(bodyLine);
        }
        return ans.toString();
    }
    /**
     * This makes the header line, e.g. 0|1|2|3|4\n
     * @return the String that is the header line for the given board
     */
    String makeHeader() {
        StringBuilder ans = new StringBuilder("  "); // README shows two spaces at
        String sep=""; //start with nothing to separate, then switch to | to separate
        for (int i = 0; i < toDisplay.getWidth(); i++) {
            ans.append(sep);
            ans.append(i);
            sep = "|";
        }
        // ans.append("  ");
        ans.append("\n");
        return ans.toString();
    }
}
