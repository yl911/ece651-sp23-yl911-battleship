package battleship;

public interface Board<T> {
    public int getWidth();
    public int getHeight();
    public String tryAddShip(Ship<T> toAdd);
    public T whatIsAtForSelf (Coordinate where);
    public T whatIsAtForEnemy(Coordinate where);
    public Ship<T> fireAt(Coordinate where);
    public boolean AreShipsAllSunk ();
}
