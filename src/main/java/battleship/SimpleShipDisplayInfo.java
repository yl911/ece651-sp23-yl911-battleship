package battleship;

public class SimpleShipDisplayInfo<T> implements ShipDisplayInfo<T>{
    private T myData;
    private T onHit;
    public SimpleShipDisplayInfo (T inputData, T isHit) {
        this.myData = inputData;
        this.onHit = isHit;
    }
    // getInfo check if (hit) and returns onHit if so, and myData otherwise.
    public T getInfo (Coordinate where, boolean hit) {
        //TODO this is not right.  We need to
        //the variable where is not used
        if (hit) {
            return onHit;
        } else {
            return myData;
        }
    }
}
