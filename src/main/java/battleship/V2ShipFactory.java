package battleship;

public class V2ShipFactory implements AbstractShipFactory<Character>{
    protected Ship<Character> createRectangleShip(Placement where, int w, int h, char letter, String name) {
        if (where.getOrientation() == 'V') {
            return new RectangleShip<Character>
                    (name, where.getCoordinate(), w, h, letter, '*');
        } else if (where.getOrientation() == 'H') {
            return new RectangleShip<Character>
                    (name, where.getCoordinate(), h, w, letter, '*');
        } else {
            throw new IllegalArgumentException("The direction of Submarine or Destroyer is illegal");
        }
    }
    @Override
    public Ship<Character> makeSubmarine(Placement where) {
        return createRectangleShip(where, 1, 2, 's', "Submarine");
    }

    @Override
    public Ship<Character> makeDestroyer (Placement where) {
        return createRectangleShip(where, 1, 3, 'd', "Destroyer");
    }
    @Override
    public Ship<Character> makeBattleship(Placement where) {
        return new nonRectangleShip<Character>
                (where.getCoordinate(), "Battleship", where.getOrientation(), 'b', '*');
    }

    @Override
    public Ship<Character> makeCarrier (Placement where) {
        return new nonRectangleShip<Character>
                (where.getCoordinate(), "Carrier", where.getOrientation(), 'c', '*');
    }
}
