package battleship;

import java.util.HashSet;

public class nonRectangleShip<T> extends BasicShip<T> {
    final String name;
    public String getName() {
        return this.name;
    }
    // generate coordinate, hard coding
    static HashSet<Coordinate> makeCoords(Coordinate upperLeft, String name, char direction) {
        HashSet<Coordinate> ans = new HashSet<Coordinate>();
        if (name.equals("Battleship")) {
            if (direction == 'U') {
                ans.add(new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 1));
                ans.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn()));
                ans.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1));
                ans.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 2));
            } else if (direction == 'R') {
                ans.add(new Coordinate(upperLeft.getRow(), upperLeft.getColumn()));
                ans.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn()));
                ans.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1));
                ans.add(new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn()));
            }else if (direction == 'D') {
                ans.add(new Coordinate(upperLeft.getRow(), upperLeft.getColumn()));
                ans.add(new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 1));
                ans.add(new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 2));
                ans.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1));
            }else if (direction == 'L') {
                ans.add(new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 1));
                ans.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn()));
                ans.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1));
                ans.add(new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn() + 1));
            }
        } else if (name.equals("Carrier")) {
            if (direction == 'U') {
                ans.add(new Coordinate(upperLeft.getRow(), upperLeft.getColumn()));
                ans.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn()));
                ans.add(new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn()));
                ans.add(new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn() + 1));
                ans.add(new Coordinate(upperLeft.getRow() + 3, upperLeft.getColumn()));
                ans.add(new Coordinate(upperLeft.getRow() + 3, upperLeft.getColumn() + 1));
                ans.add(new Coordinate(upperLeft.getRow() + 4, upperLeft.getColumn() + 1));
            } else if (direction == 'R') {
                ans.add(new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 1));
                ans.add(new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 2));
                ans.add(new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 3));
                ans.add(new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 4));
                ans.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn()));
                ans.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1));
                ans.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 2));
            }else if (direction == 'D') {
                ans.add(new Coordinate(upperLeft.getRow(), upperLeft.getColumn()));
                ans.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn()));
                ans.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1));
                ans.add(new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn()));
                ans.add(new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn() + 1));
                ans.add(new Coordinate(upperLeft.getRow() + 3, upperLeft.getColumn() + 1));
                ans.add(new Coordinate(upperLeft.getRow() + 4, upperLeft.getColumn() + 1));
            }else if (direction == 'L') {
                ans.add(new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 2));
                ans.add(new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 3));
                ans.add(new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 4));
                ans.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn()));
                ans.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1));
                ans.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 2));
                ans.add(new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 3));
            }
        }
        return ans;
    }

    public nonRectangleShip(Coordinate upperLeft, String name, char direction, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo) {
        super(makeCoords(upperLeft, name, direction), myDisplayInfo, enemyDisplayInfo);
        this.name = name;
    }
    public nonRectangleShip(Coordinate upperLeft, String name, char direction, T data, T onHit) {
        this(upperLeft, name, direction, new SimpleShipDisplayInfo<T>(data, onHit), new SimpleShipDisplayInfo<T>(null, data));
    }
}
