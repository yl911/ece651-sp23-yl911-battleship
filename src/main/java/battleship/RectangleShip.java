package battleship;

import java.util.HashSet;

public class RectangleShip<T> extends BasicShip<T>{
    final String name;
    public String getName() {
        return this.name;
    }

    static HashSet<Coordinate> makeCoords(Coordinate upperLeft, int width, int height) {
        HashSet<Coordinate> ans = new HashSet<Coordinate>();
        for (int row = 0; row < height; row++) {
            for (int column = 0; column < width; column++) {
                ans.add(new Coordinate(upperLeft.getRow() + row, upperLeft.getColumn() + column));
            }
        }
        return ans;
    }
    public RectangleShip(String name, Coordinate upperLeft, int width, int height, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo) {
        super(makeCoords(upperLeft, width, height), myDisplayInfo, enemyDisplayInfo);
        this.name = name;
    }
    public RectangleShip(String name, Coordinate upperLeft, int width, int height, T data, T onHit) {
        this(name, upperLeft, width, height, new SimpleShipDisplayInfo<T>(data, onHit), new SimpleShipDisplayInfo<T>(null, data));
    }
    // convenience constructors, make a RectangleShip with one square
    public RectangleShip(Coordinate upperLeft, T data, T onHit) {
        this("testShip", upperLeft, 1, 1, data, onHit);
    }

}
