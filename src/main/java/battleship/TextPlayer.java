package battleship;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.function.Function;

public class TextPlayer {
    private final Board<Character> theBoard;
    private final BoardTextView view;
    private final BufferedReader inputReader;
    private final PrintStream out;
    final AbstractShipFactory<Character> shipFactory;
    final String name;
    final ArrayList<String> shipsToPlace;
    final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;
    private final possibleAction possact;
    public BoardTextView getView() {
        return view;
    }

    public Board<Character> getTheBoard() {
        return theBoard;
    }

    public TextPlayer (String name, Board<Character> theBoard,
                       BufferedReader inputSource, PrintStream out, AbstractShipFactory<Character> shipFactory) {
        this.name = name;
        this.theBoard = theBoard;
        this.view = new BoardTextView(theBoard);
        this.inputReader = inputSource;
        this.out = out;
        this.shipFactory = shipFactory;
        this.shipsToPlace = new ArrayList<String>();
        this.shipCreationFns = new HashMap<String, Function<Placement, Ship<Character>>>();
        this.possact = new possibleAction();
        setupShipCreationList();
        setupShipCreationMap();
    }
    protected void setupShipCreationMap() {
        shipCreationFns.put("Submarine", (p) -> shipFactory.makeSubmarine(p));
        shipCreationFns.put("Destroyer", (p) -> shipFactory.makeDestroyer(p));
        shipCreationFns.put("Battleship", (p) -> shipFactory.makeBattleship(p));
        shipCreationFns.put("Carrier", (p) -> shipFactory.makeCarrier(p));
    }

    protected void setupShipCreationList() {
        shipsToPlace.addAll(Collections.nCopies(2, "Submarine"));
        shipsToPlace.addAll(Collections.nCopies(3, "Destroyer"));
        shipsToPlace.addAll(Collections.nCopies(3, "Battleship"));
        shipsToPlace.addAll(Collections.nCopies(2, "Carrier"));
    }
    //TODO: wrap in a class called textReader
    public Placement readPlacement(String prompt) throws IOException {
        this.out.print(prompt);
        String s = inputReader.readLine();
        if (s == null) {
            throw new EOFException();
        }
        return new Placement(s);
    }
    public Coordinate readCoordinate (String prompt) throws IOException {
        this.out.print(prompt);
        String s = inputReader.readLine();
        Coordinate c = new Coordinate(s);
        if (c.getColumn() >= theBoard.getWidth() || c.getRow() > theBoard.getHeight()) {
            throw new IllegalArgumentException();
        }
        return c;
    }
    public char readActionType (String prompt) throws IOException {
        out.print(prompt);
        String s = inputReader.readLine().toUpperCase();
        if (s.length() != 1) {
            throw new IllegalArgumentException();
        }
        char c = s.charAt(0);
        if (possact.typeSigns.contains("" + c)) {
            throw new IllegalArgumentException();
        }
        return c;
    }
    public void doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) throws IOException {
        String prompt = "--------------------------------------------------------------------------------\n" +
                        "Player " + name + " where do you want to place a " + shipName + "?\n" +
                        "--------------------------------------------------------------------------------\n";
        //TODO: simplify the outer while and merge in only one while
        while (true) {
            Placement p;
            Ship<Character> bs;
            while (true) {
                try {
                    p = readPlacement(prompt);
                    bs = createFn.apply(p);
                    break;
                } catch (IllegalArgumentException e) {
                    String s = "That placement is invalid: it does not have the correct format.\n";
                    out.print(s);
                }
            }
            String s = theBoard.tryAddShip(bs);
            if (s != null) {
                out.print(s);
                continue;
            }
            break;
        }

        out.print(view.displayMyOwnBoard());
    }

    public void possibleAction (Board<Character> enemyBoard) throws IOException{
        char c;
        while (true) {
            try {
                c = readActionType(possact.prompt(name));
                if (possact.typeSigns.indexOf(c) == 0) {
                    doOneFire(enemyBoard);
                }
                if (possact.typeSigns.indexOf(c) == 1) {
                    if (possact.moveRemain()) {
                        possact.move();
                    } else {
                        throw new IllegalArgumentException();
                    }
                }
                if (possact.typeSigns.indexOf(c) == 2) {
                    if (possact.scanRemain()) {
                        possact.sonarScan();
                    } else {
                        throw new IllegalArgumentException();
                    }
                }
                break;
            } catch (IllegalArgumentException e) {
                String s = "That action type is invalid: it does not have the correct sign.\n";
                out.print(s);
            }
        }

    }
    // TODO: move this function to possibleAction
    public void doOneFire(Board<Character> enemyBoard) throws IOException {
        String dashLine = "--------------------------------------------------------------------------------\n";
        String prompt =  dashLine + "Player " + name + " where do you want to fire at?\n" + dashLine;
        Coordinate c;
        while (true) {
            try {
                c = readCoordinate(prompt);
                break;
            } catch (IllegalArgumentException e) {
                String s = "That Coordinate is invalid: it does not have the correct format.\n";
                out.print(s);
            }
        }
        Ship<Character> sh = enemyBoard.fireAt(c);
        if (sh != null) {
            out.print(dashLine + "You hit a " + sh.getName() + "!\n" + dashLine);
        } else {
            out.print(dashLine + "You missed!\n" + dashLine);
        }
    }
    public void doPlacementPhase() throws IOException {
        out.print(view.displayMyOwnBoard());
        String inst = "--------------------------------------------------------------------------------\n" +
                    "Player " + name + " : you are going to place the following ships (which are all\n" +
                    "rectangular). For each ship, type the coordinate of the upper left\n" +
                    "side of the ship, followed by either H (for horizontal) or V (for\n" +
                    "vertical).  For example M4H would place a ship horizontally starting\n" +
                    "at M4 and going to the right.  You have\n" +
                    "\n" +
                    "2 \"Submarines\" ships that are 1x2\n" +
                    "3 \"Destroyers\" that are 1x3\n" +
                    "3 \"Battleships\" that are 1x4\n" +
                    "2 \"Carriers\" that are 1x6\n" +
                    "--------------------------------------------------------------------------------\n";
        out.print((inst));
        for (String s : this.shipsToPlace) {
            doOnePlacement(s, this.shipCreationFns.get(s));
        }
    }

    /**
     * put "this" view's board's "my own board" on the left, and enemyView's "enemy board" on the
     * right.
     */
    public String displayMyBoardWithEnemyNextToIt(BoardTextView enemyView, String myHeader, String enemyName) {
        String [] myBoard = this.view.displayMyOwnBoard().split("\n");
        String [] enemyBoard = enemyView.displayEnemyBoard().split("\n");
        String enemyHeader = "Player " + enemyName + "'s ocean";
        StringBuilder ans = new StringBuilder();
        String space = "                ";
        ans.append("     ");
        ans.append(myHeader);
        // ans.append("            ").append(space);
        int length = 2 * this.theBoard.getWidth() + 7 + enemyHeader.length();
        ans.append(String.format("%1$" + length + "s", enemyHeader));
        //ans.append(enemyHeader);
        ans.append("\n");

        ans.append(myBoard[0]);
        ans.append(space+"  ");
        ans.append(enemyBoard[0]);
        ans.append("\n");
        for (int i = 1; i < myBoard.length - 1; i++) {
            ans.append(myBoard[i]);
            ans.append(space);
            ans.append(enemyBoard[i]);
            ans.append("\n");
        }
        ans.append(myBoard[myBoard.length - 1]);
        ans.append(space+"  ");
        ans.append(enemyBoard[myBoard.length - 1]);
        ans.append("\n");
        return ans.toString();
    }
    protected boolean isLose() {
        return this.theBoard.AreShipsAllSunk();
    }
    public boolean checkWinOrLose (String anotherPlayerName) {
        if (isLose()) {
            out.print("Player " + anotherPlayerName + " wins.\n");
            return true;
        }
        return false;
    }
    public void playOneTurn (Board<Character> enemyBoard, BoardTextView enemyView, String enemyName) throws IOException {
        out.print("Player " + name + "'s turn:\n");
        String myHeader = "Your ocean";
        out.print(displayMyBoardWithEnemyNextToIt(enemyView, myHeader, enemyName));
        if (possact.moveRemain() || possact.moveRemain()) {
            possibleAction(enemyBoard);
        } else {
            doOneFire(enemyBoard);
        }

    }
}
