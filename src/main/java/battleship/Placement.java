package battleship;
import java.lang.Character;
public class Placement {
    private final Coordinate coordinate;
    private final char orientation;
    public Placement (Coordinate where, char orientation) {
        this.coordinate = where;
        this.orientation = Character.toUpperCase(orientation);
    }
    public Placement (String descr) {
        if (descr.length() != 3) {
            throw new IllegalArgumentException("Placement must be three characters but is " + descr.length() + " bytes long");
        }
        String descrIgnoredCases = descr.toUpperCase();
        this.coordinate = new Coordinate(descrIgnoredCases.substring(0,2));
        String possDirec = "VHURDL";
        if (!possDirec.contains("" + descrIgnoredCases.charAt(2))) {
            throw new IllegalArgumentException("Placement is illegal and is " + descrIgnoredCases.charAt(2));
        }
//        if (descrIgnoredCases.charAt(2) != 'V' && descrIgnoredCases.charAt(2) != 'H') {
//            throw new IllegalArgumentException("Placement must be horizontal or vertical but is " + descrIgnoredCases.charAt(2));
//        }
        this.orientation = descrIgnoredCases.charAt(2);
    }

    public char getOrientation() {
        return orientation;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }
    @Override
    public boolean equals(Object o) {
        if (o.getClass().equals(this.getClass())) {
            Placement c = (Placement) o;
            return this.orientation == c.getOrientation() && this.coordinate.equals(c.getCoordinate());
        }
        return false;
    }
    @Override
    public String toString() {
        return coordinate.toString() + " " + orientation;
    }
    @Override
    public int hashCode() {
        return toString().hashCode();
    }
}
