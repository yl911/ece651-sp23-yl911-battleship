package battleship;

import java.util.ArrayList;
import java.util.HashSet;

public class BattleShipBoard<T> implements Board<T>{
    private final int width;
    private final int height;

    private final ArrayList<Ship<T>> myShips;
    private final PlacementRuleChecker<T> placementChecker;
    //todo : private?
    HashSet<Coordinate> enemyMisses;
    final T missInfo;

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }
    public PlacementRuleChecker<T> getPlacementChecker() {
        return placementChecker;
    }

    /**
     * Constructs a BattleShipBoard with the specified width
     * and height
     * @param width is the width of the newly constructed board.
     * @param height is the height of the newly constructed board.
     * @throws IllegalArgumentException if the width or height are less than or equal to zero.
     */
    public BattleShipBoard (int width, int height, PlacementRuleChecker<T> placementChecker, T missInfo) {
        if (width <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's width must be positive but is " + width);
        }
        if (height <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's height must be positive but is " + height);
        }
        this.height = height;
        this.width  = width;
        this.myShips = new ArrayList<Ship<T>>();
        this.placementChecker = placementChecker;
        this.enemyMisses = new HashSet<Coordinate>();
        this.missInfo = missInfo;
    }
    public BattleShipBoard(int w, int h, T missInfo) {
        this(w, h, new NoCollisionRuleChecker<>(new InBoundsRuleChecker<T>(null)), missInfo);
    }
    public String tryAddShip(Ship<T> toAdd) {
        String s = placementChecker.checkPlacement(toAdd, this);
        if (s == null) {
            this.myShips.add(toAdd);
            return null;
        }
        return s;
    }
    public T whatIsAtForSelf(Coordinate where) {
        return whatIsAt(where, true);
    }
    public T whatIsAtForEnemy(Coordinate where) {
        return whatIsAt(where, false);
    }
    protected T whatIsAt(Coordinate where, boolean isSelf) {
        for (Ship<T> s: myShips) {
            if (s.occupiesCoordinates(where)){
                return s.getDisplayInfoAt(where, isSelf);
            }
        }
        if (!isSelf && enemyMisses.contains(where)) {
            return missInfo;
        }
        return null;
    }
    public Ship<T> fireAt(Coordinate where) {
        for (Ship<T> s: myShips) {
            if (s.occupiesCoordinates(where)){
                s.recordHitAt(where);
                return s;
            }
        }
        enemyMisses.add(where);
        return null;
    }
    public boolean AreShipsAllSunk () {
        for (Ship<T> s : myShips) {
            if (!s.isSunk()) {
                return false;
            }
        }
        return true;
    }
}

